#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Base
{
public:
    void memfcn(Base &b) { b = *this; }
    void pub_mem();

protected:
    int prot_mem;

private:
    char priv_mem;
};
struct Pub_Derv : public Base
{
    void memfcn(Base &b) { b = *this; }
    int f() { return prot_mem; }
};
struct Priv_Derv : private Base
{
    void memfcn(Base &b) { b = *this; }
    int f() { return prot_mem; }
};

struct Prot_Derv : protected Base
{
    void memfcn(Base &b) { b = *this; }
    int f() { return prot_mem; }
};
struct Derived_from_Public : public Pub_Derv
{
    void memfcn(Base &b) { b = *this; }
    int use_base() { return prot_mem; }
};
struct Derived_from_Protected : public Prot_Derv
{
    void memfcn(Base &b) { b = *this; }
    int use_base() { return prot_mem; }
};
struct Derived_from_Private : public Priv_Derv
{
    // void memfcn(Base &b) { b = *this; }
    //int use_base() { return prot_mem; }
};
int main()
{

    return 0;
}