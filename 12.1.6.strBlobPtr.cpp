#include <memory>
#include <list>
#include <vector>
#include <string>
#include <new>
#include <iostream>
#include <fstream>
using namespace std;


class strBlobPtr;
class constStrBlobPtr;
class strBlob
{
    friend class strBlobPtr;
    friend class constStrBlobPtr;
public:
    /// 注意这里的函数体不能直接写在这，因为目前 strBlobPtr还是一个不完全类型
    strBlobPtr begin();// {return strBlobPtr(*this);}
    strBlobPtr end();
    constStrBlobPtr cbegin();// {return constStrBlobPtr(*this);}
    constStrBlobPtr cend();
    typedef std::vector<std::string>::size_type size_type;
    strBlob() : data(make_shared<vector<string>>()) {}
    strBlob(std::initializer_list<std::string> il) : data(make_shared<vector<string>>(il)){};
    size_type size() const { return data->size(); }
    bool empty() const { return data->empty(); }
    /// 添加元素
    void push_back(const std::string &t) { data->push_back(t); }
    void pop_back()
    {
        check(0, "pop on empty strBlob");
        data->pop_back();
    }
    /// 如果 data[i] 不合法,抛出一个异常
    void check(size_type i, const std::string &msg) const
    {
        if (i >= data->size())
            throw std::out_of_range(msg);
    }
    /// 元素访问
    std::string &front()
    {
        /// 为空则抛异常
        check(0, "front on empty strBlob");
        return data->front();
    }
    /// 声明 const 属性, 表示不会改变内容
    std::string &front() const
    {
        /// 为空则抛异常
        check(0, "front on empty strBlob");
        return data->front();
    }
    std::string &back()
    {
        check(0, "back on empty strBlob");
        return data->back();
    }
    
private:
    std::shared_ptr<std::vector<std::string>> data;
};
class strBlobPtr
{
public:
    strBlobPtr():curr(0){};
    strBlobPtr(strBlob &a, size_t sz = 0):wptr(a.data),curr(sz){};
    std::string &deref() const {
        //auto p = check(curr, "dereference past end");
        //return (*p)[curr];
        return (*check(curr, "dereference past end"))[curr];
    }
    strBlobPtr &incr() { // 前缀递增??
        check(curr, "increment past end of strBlobPtr");
        ++curr;
        return *this;
    }
    std::size_t cur(){return curr;};
    ~strBlobPtr(){};
    std::string &operator*() const {
        auto p = check(curr, "dereference past end");
        return (*p)[curr];
    }
    std::string *operator->() const {
        return &this->operator*();
    }
private:
    std::shared_ptr<std::vector<std::string> >
        check(std::size_t i, const std::string &msg) const {
            auto ret = wptr.lock();
            if(!ret)
                throw std::runtime_error("unbound strBlobPtr");
            if(i >= ret->size())
                throw std::out_of_range(msg);
            return ret;
        }
    std::weak_ptr<std::vector<std::string> > wptr;
    std::size_t curr;   //数组中当前的位置
};
class constStrBlobPtr
{
public:
    constStrBlobPtr():curr(0){};
    constStrBlobPtr(const strBlob &a, size_t sz = 0):wptr(a.data),curr(sz){};
    std::string &deref() const {
        //auto p = check(curr, "dereference past end");
        //return (*p)[curr];
        return (*check(curr, "dereference past end"))[curr];
    }
    constStrBlobPtr &incr() { // 前缀递增??
        check(curr, "increment past end of constStrBlobPtr");
        ++curr;
        return *this;
    }
    std::size_t cur(){return curr;};
    ~constStrBlobPtr(){};
private:
    std::shared_ptr<std::vector<std::string> >
        check(std::size_t i, const std::string &msg) const {
            auto ret = wptr.lock();
            if(!ret)
                throw std::runtime_error("unbound constStrBlobPtr");
            if(i >= ret->size())
                throw std::out_of_range(msg);
            return ret;
        }
    std::weak_ptr<std::vector<std::string> > wptr;
    std::size_t curr;   //数组中当前的位置
};
strBlobPtr strBlob::begin() {return strBlobPtr(*this);}
strBlobPtr strBlob::end() {
    auto ret = strBlobPtr(*this, data->size()); return ret;
}
constStrBlobPtr strBlob::cbegin() {return constStrBlobPtr(*this);}
constStrBlobPtr strBlob::cend() {
    auto ret = constStrBlobPtr(*this, data->size()); return ret;
}
int main()
{
    ifstream ifs("main.cxx");
    strBlob sb;
    // string s;
    // while(getline(ifs, s)){
    //     sb.push_back(s);
    // }
    // constStrBlobPtr test((const strBlob)sb);
    // for(constStrBlobPtr sbp = sb.cbegin(); sbp.cur() != sb.cend().cur(); sbp.incr()) {
    //     cout<< sbp.deref() <<endl;
    // }
    sb.push_back("111");
    sb.push_back("222");
    sb.push_back("333");
    strBlobPtr test(sb);
    cout<<*test<<endl;
    cout<<*(test.operator->())<<endl;
    return 0;
}