
## 2、变量和基本类型
- 1.算术类型
   - C++规定sizeof(int)>=sizeof(short)，sizeof(long)>=sizeof(int)，sizeof(long long)>sizeof(long)，long long是C++11中新定义的
   - double比float扩展了精度值，计算代价相差无几甚至更快，long double基本没必要的且带来的耗时也不容忽视
   - 需要注意的是，char、signed char、unsigned char三者是不一样的，虽然char会表现为后两者之一，但具体是由编译器决定的
   - C++没有约定带符号的类型如何表示但约定了正值和负值的量应该平衡
<br>
  - > 无法预知和依赖于实现环境的行为：无法预知的行为源于编译器无须(有时是不能)检测的错误，程序也应尽量避免依赖于实现环境的行为,如把int的尺寸看做是确定不变的,看下表。
<br>[原文：https://sourceforge.net/p/predef/wiki/DataModels/](https://sourceforge.net/p/predef/wiki/DataModels/)
    >|Data type|LP32|ILP32|	LP64|LLP64|ILP64|SILP64|
    >|--|--|--|--|--|--|--|
    >|char|	8|	8|	8|	8|	8|	8|
    >|short|	16|	16|	16|	16|	16|	64|
    >|int|	16|	32|	32|	32|	64|	64|
    >|long|	32|	32|	64|	32|	64|	64|
    >|long long|||	64|	64|	64|	64|
    >|pointer|32|	32|	64|	64|	64|	64|
   - 1.2.字面值常量
      - 如果两个字符串字面值位置紧邻且仅由空格、缩进和换行符分隔，则实际是一个整体。
      - 转义序列
         - ```\a```,报警响铃符，```\b```,退格符，```\f```,进纸符，```\v```，纵向制表符
         - 泛化转义序列：```\x```后紧跟1个或多个十六进制数，或```\```后紧跟```1/2/3```个8进制数
      - 指定字面值类型：前缀(```u/U/L/u8```)、后缀(```uU lL(long/long Double) fF llLL ```)
- 2.变量
> C++的每个变量都有类型，决定了所占空间大小和布局方式和能参与的运算
   - 变量的定义(下面4种初始化方式都是可以的)
      ```c
      int a1 = 0;
      int a2 = {0};
      int a3{0};  //列表初始化，存在丢失风险时将报错
      int a4(0);
      ```
   - 变量声明和定义的关系
      - 声明规定类型和名字，定义还申请空间、赋值
         - 声明：由一个基本类型(base type)和一个声明符(declarator)列表组成
   - 2、复合类型
      - 2.1 引用：为对象起+ 90另外一个名字
         ```c++
         int ival = 1024;
         int &refVal = ival;
         ```
      - 2.2 指针，实现了对其他对象的间接引用
         - 空指针
            - ：char *p1 = nullptr，*p2 = 0,*p3 = NULL;都可生成空指针
            - nullptr是一种特殊类型的字面值
            - NULL是预处理变量，和0一样
         - void *指针：可存放任意对象的地址
      - 2.3 复合类型申明
         > 变量定义：基本数据类型+一组声明符(类型修饰符)
         <br>int i=0,*p=&i,&r=i; //一条定义语句，一个基本数据类型可定义出不同类型的变量
         - 指向指针的引用：引用不是对象，所以不能定义指向引用的指针，但指针是对象```(从右向左读)```
           > int *p,*&r=p;//r是对指针p的引用
      - 2.4 const 限定符
        > 默认情况下，const对象仅在文件内有效，C++允许：对于connst变量不管是声明还是定义都加上extern关键字```(gcc编译会报错)``` 
         - const的引用：```const int &r1 = ci;//不能修改绑定的对象```
         - 引用的类型必须与其所引用的对象一致，```except ```①初始化常量引用时允许用任意表达式做初始值 ②允许常量引用绑定非常量对象、字面值、表达式
         - 常量表达式：值不变，编译过程中能得到计算结果
           > const int a = 1;//常量表达式
         <br>int b = 1;//非常量表达式
         <br>const int c = getSize();//非常量表达式(运行时才能获取值)
         - 可用```constexpr```修饰常量表达式
            - 指针必须被声明成nullptr或0
            - 函数体内定义的变量不存放在固定地址,不能用它修饰
            - ```const     int *p = nullptr; //指向整型常量的指针```
            - ```constexpr int *q = nullptr; //指向整数的常量指针，等效于 int *const q```
      - 2.5 处理类型
         - 类型别名
            ```c
            typedef char * pstring;
            const pstring p1 = 0;
            const char *p2 = 0;//注意这个类型和上面的不一样
            ```
         - auto类型说明符
            > 通过初始值推断变量的类型，会忽略顶层const，保留底层const
         - decltype类型指示符
            > 选择并返回操作数的数据类型(包括顶层const和引用在内)
      - 2.6 自定义数据结构
      
### 3.1 命名空间的using声明
- >形式：using namespace::name;

---
### 3.2 标准库类型string
- >表示可变长的字符序列
- size()的返回值值类型是string::size_type，一个无符号的整数
- string对象和字符字面值及字符串字面值混在一条语句中使用时，必须确保每个加法运算符两侧对象至少一个时string，如：
   > string s6 = s1 + ", " + "world";//正确
   ><br>string s7 = "hello" + "," +s1;  //错误
- ```<cctype>```中定义了函数isalnum()、isdigit()...
- 对string对象的每个元素做某个操作，如
   ```c
   for (auto &c : s)    //基于范围的for语句
      c = toupper(c);   //改成大写
   ```
- 使用下标方式迭代：
   ```
   for(decltype(s.size()) index=0;index!=s.size()
      &&!isspace(s[index]); ++index)
      s[index] = toupper(s[index]);
   ```
- string 对象上的操作
   - 1.初始化对象的方式
   - 2.所能执行的操作：函数名调用操作、<<、+等运算符操作
      - ```string s;cin>>s1>>s2;```:  会自动忽略开头的空白(\n \t)
      - ```while(getline(cin,s));```:遇到换行符结束但不会存到s中,返回流参数

### 3.2 标准库类型vector
> 表示对象的集合，也叫容器,是一个类模板
><br>#include \<vector\> <br>using std::vector
- 编译器根据模板创建类或函数的过程称为实例化
- 初始化如：
   ```c
   vector<T> v5{a,b,c,d,...}
   vector<T> v5={a,b,c,d,..}  //注意这里不是圆括号
   ```
### 3.4 迭代器
- 有效的迭代器或者指向某个元素、或者指向容器中尾元素的下一位置。
- 有迭代器的类型同时拥有返回迭代器的成员，比如都有begin/cbegin()和end/cend()成员
   - *iter:返回迭代器所指元素的引用
   - iter->mem:解引用该元素，(*iter).mem
   - ...
- 迭代器类型：
   ```cpp
   vector<int>::iterator it;  // it能读写vector<int>的元素
   string::iterator it;  // it能读写string对象中的元素
   vector<int>::const_iterator it;  // it能读不能写vector<int>的元素
   string::const_iterator it;  // it能读不能写string对象的元素
   ```
- ```谨记：但凡是使用了迭代器的循环体，都不要向迭代器所属的容器中添加元素```
- 迭代器的运算：iter+-n表向前后移动n个元素，iter1-iter2表之间的距离(```difference_type```)，
- 例：
   ```cpp
   auto beg = text.begin(),end = text.end();
   auto min = text.begin() + (end - beg)/2;
   while(mid != end && *mid != sought){
      if(sought < *mid) end = mid;
      else beg = mid+1;
      mid = beg + (end-beg)/2;
   }
   ```
### 3.5 数组
- 复杂的数组声明
   - int *ptrs[10];  //ptrs是含有10个整型指针的数组
   - int &refs[10] = /\* ? \*/   //错误，不存在引用的数组
   - int (*pArray)[10] = &arr;   //指向含有10个整数的数组，从内向外理解
   - int (&arrRef)[10] = arr;    //引用一个含10个整数的数组
   - int *(&array)[10] = ptrs;   //array是数组的引用，该数组含有10个指针
- 数组下标为定义在```<cstddef>```中的```size_t```类型
- 数组与指针：
   ```cpp
   int ia[]={0,1,2};
   auto ia2(ia);  //ia2是一个整型指针，指向ia的第一个元素
   decltype(ia) ia3 = {0,1,2};//ia3是数组
   int *beg=begin(ia);  //指向ia的首元素
   int *last=end(ia);   //指向ia尾元素的下一位置的指针
   ```
- 与旧代码的接口：
   - 允许使用空字符结束的字符数组来初始化string对象或为string对象赋值
   - const char *str=s.c_str();//不能保证返回的数组一直有效
   - 数组初始化vector对象：
      ```cpp
      int int_arr[]={0,1,2,3,4,5};
      vector<int> ivec(begin(int_arr),end(int_arr));
      ```

## 4.表达式
### 4.11 类型转换
- 隐式类型转换
   - 表达式中，比int小的会提升为较大的整数类型
   - 条件中，非布尔会转换成布尔类型
   - 初始化过程中，初始值转换成变量类型
   - 算术转换：
      - 运算对象会转换成最宽的类型
      - 整数值会被转换成浮点类型
   - 整型提升
      - char、uchar、short、ushort会转int,不能存则转unsigned int
      - 较大的char(wchar_t、char16_t、char32_t)会转成int、uint、ulong、llong、ullong中最小的能容纳原类型值得一个
   - 无符号类型得运算对象
      - 先进行整型提升
      - 无符号>=带符号(类型大小)，对象转为无符号
      - 无符号<带符号(类型大小)，如 unsigned int a,long b,若a<=b,a转b,否则转a
- 显示转换：
   - 形式：```cast-name<type>(expression)```
      - **type**是目标
      - **expression**是要转换的值
      - **case-name**是```①static_case、②dynamic_case、③const_cast、④reinterpret_cast```中的一种
         - ①.任何具有明确定义的类型转换
            ```cpp
            double s=static_case<double>(j)/i;
            ```
         - ②.只能改变运算对象的底层const
            ```cpp
            const char *pc;
            char *p=const_cast<char*>(pc);//正确但通过p写行为未定义
            ```
         - ③为运算对象的位模式提供较低层次上的重新解释
            ```cpp
            int *ip;
            char *pc=reinterpret_cast<char*>(ip);//很危险
            string str(pc);//可能产生错误
            ```

### 4.12 运算符的优先级表
- p148页

---
## 5.语句
### 5.6 TRY语句块和异常处理
> 异常：存在运行时的反常行为，超出了函数的正常功能范围，如失去数据库连接、遇到意外输入。---设计系统最难的一部分
- throw表达式：用来表示遇到了无法处理的问题，我们说throw引发了异常
- try语句块：用它处理异常，以try语句块开始，多个catch子句结束。我们称为异常处理代码。
- 一套异常类：用于在throw和相关catch子句之间传递异常的具体信息
- 例：
   ```cpp
   #include <stdexcept>
   while(cin>>item1>>item2) {
      try{
         if(item1.isbn() != item2.isbn())
            throw runtime_error("Date must refer to same ISBN");
      } catch (runtime_error err) {
         // 提醒用户两个ISBN必须一致，询问是否重新输入
         cout<<err.what()
            <<"\nTry Again? Enter y or n"<<endl;
            char c;
            cin>>c;
            if(!cin || c == 'n') break;
      }
   }
   ```
- 标准异常：
   - \<exception\>定义了最通用的异常类，只报告异常发生
   - \<stdexcept\>定义了常用的异常类
   - \<new>定义了bad_alloc异常类型
   - \<type_info>定义了bad_cast异常类型

---
## 6.函数
> - 组成：return value、function name、parameter、\<body\><br>
> - 名字有作用域，对象有生命周期

### 6.2.6 含有可变形参的函数
- initializer_list 形参
   - 与vector不一样的是，initializer_list对象中的元素永远是常量值，无发改变
      ```cpp
      void error_msg(ErrCode e,initializer_list<string> il){
         cout<<e.mesg()<<": ";
         for(auto beg=il.begin();beg!=il.end();++beg)
            cout<<*beg<<" ";
         cout<<endl;
      }
      //调用：
      error_msg(ErrCode(42),{"test1","test2"});
      ```
- 省略符形参
   - 只能出现在形参列表的最后一个位置
   - 你的C编译文档会教你怎么使用varargs

- 值是怎么返回的
   - 返回的值用于初始化调用点的一个临时变量，该临时变量就是函数调用的结果
   - 不要返回局部对象的引用或指针
- 列表初始化返回值
   - C++ 11规定函数可以返回花括号包围的值的列表，如：
      ```c
      vector<string> process(){
         return {"aa","bb"};
      }
      ```
- 主函数main的返回值
   - 没有语句隐式返回0
   - \<cstdlib\>中定了两个预处理变量：```EXIT_FAILURE```、```EXIT_SUCCESS```
- 返回数组指针的函数:数组维度必须跟在函数名后，形参列表跟在函数名后且先于数组维度
   ```c
   Type (*function(parameter_list))[dimension]
   int (*func(int i))[10];
   ```
- 使用尾置返回类型
   > 尾置返回类型跟在形参列表后，```->```开头，原来的位置替换位auto,如：
   ```c
   auto func(int i) -> int(*)[10];
   ```
- 使用decltype，如：
   ```c
   int odd[]={1,3,5};
   int even[]={0,2,4};
   decltype(odd) *arrPtr(int i){
      return (i%2)?&odd:&even;
   }
   ```
### 6.4 函数重载
> 同一作用域内几个函数名字相同但行参列表(类型或数量)不同，称为```重载函数```
- 一个拥有顶层const的形参无法和另一个没有顶层const的形参区分开
- 如果形参是指针或引用，可通过区分其指向的是常量还是非常量实现函数重载，如：
   ```c
   Record lookup(Account&);   //用做Account的引用
   Record lookup(const Account&); //新函数,作用于常量的引用
   ```
## 7.1 抽象数据类型
```cc
struct Sales_data {
   //构造函数
   Sales_data() = default;
   Sales_data(const std::string &s):bookNo(s){}
   Sales_data(const std::string &s,unsigned n,double p):
      bookNo(s),units_sold(n),revenue(p*n){}
   Sales_data(std::istream &);

   //新成员：关于Sales_data对象的操作
   std::string isbn() const {return bookNo;}
   Sales_data &combine(const Sales_data&);
   double avg_price() const;
   //数据成员
   std::string bookNo;
   unsigned units_sold = 0;
   double revenue = 0.0;
};
// Sales_data的非成员接口函数
Sales_data add(const Sales_data&,const Sales_data&);
std::ostream &print(std::ostream&,const Sales_data&);
std::istream &read(std::istream,Sales_data&);
```
- 因为this总是指向"这个"对象,所以this是一个常量指针(不允许改变this中保存的地址)
- const成员函数
   - isbn()函数后面的const修饰隐式的this指针
   - 默认情况下，this指向类类型非常量版本的常量指针，这样的函数称为常量成员函数
   - ```常量对象,以及常量对象的引用或指针都只能调用常量成员函数```
- 类作用域和成员函数
   - 类本身是一个作用域
- 在类的外部定义成员函数
   - 定义与申明需要匹配，必须包含所属类名，如：
      ```cc
      double Sales_data::avg_prise() const {
         if(units_sold) return revenue/units_sold;
         else return 0;
      }
      ```
- 定义一个返回this对象的函数
```cc
// 内置运算符将左侧对象作为当成左值返回，故这里保持一致
Sales_data &Sales_data::combine(const Sales_data &rhs){
   units_sold += rhs.units_sold;
   revenue += rhs.revenue;
   return *this;//返回调用该函数的对象
}
//调用：
total.combine(trans);   //解引用得到total的引用
```

#### 7.1.3 定义类相关的非成员函数
> 属于类接口的组成部分但不属于类本身
```cc
// IO类属于不能被拷贝的类，只能用引用方式
istream &read(istream &is,Sales_data &item){
   double price=0;
   is>>item.bookNo>>item.units_sold>>price;
   item.revenue=price*item.units_sold;
   return is;
}
```
#### 7.1.4 构造函数
> 控制对象的初始化过程，没有返回类型，```不能被声明成const,因为创建const对象时，知道构造函数完成初始化过程，对象才取得常量属性```
- 合成的默认构造函数
   - 不提供任何构造函数则隐式定义，无需实参
   - 如果类内存在初始值则用它初始化成员，否则默认初始化

- =default
   - 定义一个默认构造函数，作用等同于默认构造函数
   - 在类内表该构造函数是内联的，类外不是
- 初始话列表
   - 冒号和花括号之间的代码称为构造函数初始化列表
   - 被初始值列表忽略的成员使用默认构造函数相同的方式初始化
   - 有些编译器不支持类内初始化
- 在类外部定义构造函数
   ```cc
   Sales_data::Sales_data(std::istream &is){
      read(is,*this);//从is中读取一条交易信息存入this中
   }
   ```
   - 该构造函数初始值列表是空的，在构造函数体内初始化成员

#### 7.1.5 拷贝、赋值和析构
- 如果类包含vector或string成员，则其拷贝、赋值或销毁的合成版本能正常工作，管理动态内存通常不能依赖于合成版本

## 7.2 访问控制与封装
- public:说明符之后的成员在整个程序内可被访问
- private:说明符之后的成员可以内类的成员函数访问
- class和struct的唯一区别是默认访问权限不同
   ```cc
   class Sales_data {
   public:  //添加访问说明符
      //构造函数
      Sales_data() = default;
      Sales_data(const std::string &s):bookNo(s){}
      Sales_data(const std::string &s,unsigned n,double p):
         bookNo(s),units_sold(n),revenue(p*n){}
      Sales_data(std::istream &);

      //新成员：关于Sales_data对象的操作
      std::string isbn() const {return bookNo;}
      Sales_data &combine(const Sales_data&);
   private:
      double avg_price() const
         {return units_sold?revenue/units_sold:0;}
      //数据成员
      std::string bookNo;
      unsigned units_sold = 0;
      double revenue = 0.0;
   };
   // Sales_data的非成员接口函数
   Sales_data add(const Sales_data&,const Sales_data&);
   std::ostream &print(std::ostream&,const Sales_data&);
   std::istream &read(std::istream,Sales_data&);
   ```
#### 7.2.1 友元
> 类可以允许其他类或函数访问它的非公有成员，方法是令其他类或函数成为它的友元
- 友元的声明仅仅指定了访问权限，如果希望类用户能调用，必须再友元声明之外再声明一次(不过很多编译器未强制限定)

## 7.3 类的其他特性
- 除了定义数据成员，类还能自定义某种类型在类中的别名(存在访问限制)
```cc
#ifndef _SCREEN_H
#define _SCREEN_H

#include <string>

class Screen{
public:
   // 必须先定义后使用
   typedef std::string::size_type pos;
   Screen() = default;//已有构造函数，不加不会自动生成默认构造函数
   Screen(pos ht,pos wd,char c):height(ht),width(wd),
      contents(ht *wd,c){}
   char get() const
      {return contents[cursor];} //去读光标处字符，隐式内联
   inline char get(pos ht,pos wd) const;  //显示内联
   Screen &move(pos r,pos c);    // 能在之后被设置为内联
   // 类型别名，与上等价
   // using pos = std::string size_type;
private:
   pos cursor = 0;
   pos height = 0, width = 0;
   std::string contents;
};
inline
Screen &Screen::move(pos r,pos c){
   pos row = r*width;
   cursor = row + c;
   return *this;
}
char Screen::get(pos r,pos c)const  //在类内声明成inline
{
   pos row = r*width;   //计算行的位置
   return contents[row + c];//返回给定列字符
}
#endif
```
- 虽然无需在声明和定义的地方同时声明inline,不过最好只在类外定义的地方说明，这样容易理解
- 成员函数也能被重载
- 可变数据成员：
   - mutable修饰的成员可在const成员函数中被修改
   ```cc
   class Window_mgr{
      std::vector<Screen> screens{Screen(24,80,' ')};
   };
   ```
   - 提供类内初始值时必须使用```符号=```或```花括号```
#### 7.3.2 返回*this的成员函数
- 如：myScreen.set('#').display(count);
#### 7.3.3 类类型
- 使用方法：
   ```cc
   Sales_data item1;
   class Sales_data item1; //等价声明，从C语言继承而来
   ```
- 类的声明：
   ```cc
   class Screen;   //前向声明，只声明不定义，不完全类型,可已指向这种类型的指针或引用，也可以声明以不完全类型为参数或者返回类型的函数
   ```
   - 只有当类全部完成后才算被定义，所以类成员不能时类自己
   - 一旦一个名字出现后，它就被认为出现过了(尚未定义)，因此允许包含指向自身类型的引用或指针

#### 7.3.4 友元再探
- 友元函数定义在类内部，隐式内联
- 如果一个类指定了友元类，则友元类的成员函数可以访问此类的所有成员(包括私有成员)
- 令成员函数作为友元：
   ```cpp
   calss Screen{
      // Window_mgr::clear 需要在之前被声明
      friend void Window_mgr::clear(ScreenIndex);
   }
   ```
   - 定义Window_mgr类，申明clear()但不能定义,使用screen前先声明
   - 定义Screen,包括对clear的友元声明
   - 定义clear
- 函数重载和友元
   - 需要对函数中的每一个分别声明
- 友元声明和作用域
   - 类和成员函数的声明不是必须出现在友元声明之前，当一个名字第一次出现在一个友元声明中时，我们隐式假定该名字在当前作用域中是可见的，然而友元本身不一定真的声明在当前作用域中，```甚至在类内部定义该函数，也必须在类外提供相应的声明```，换句话说，仅仅用声明友元函数的类的成员调用该友元函数，它也必须是被声明过的：
      ```cpp
      struct X {
         friend void f(){/*友元函数可以在类内部定义*/}
         X(){f();}   //错误，f还没被声明
         void g();
         void h();
      }
      void X::g(){return f();}//错误，f还没被声明
      void f();
      void X::h(){return f();}//正确，现在f的声明在作用域中了
      ```
      - 重要的是理解友元函数声明的作用是影响访问权限，它本身并非普通意义上的声明
      - ```有的编译器并不强制执行上述关于友元的限定```
## 7.4 类的作用域
- 每个类都会定义它自己的作用域，作用域之外，普通数据和成员只能通过对象、引用或者指针使用成员访问运算符。
   ```cpp
   Screen::pos ht=24;      //使用Screen定义的pos类型
   ```
#### 7.4.1 名字查找与类作用域
- 类的定义分成两步：
   - 编译成员声明
   - 直到类全部可见才编译函数体(所以能使用类中定义的任何名字)

## 7.5 构造函数再探
- 构造函数的初始值有时必不可少(比如const或引用)
- 成员初始化的顺序：
   - 与在类定义中出现的顺序一致，与初始化列表中出现的顺序没有关系
- 默认实参和构造函数

#### 7.5.2 委托构造函数
- 一个委托构造函数使用它所属类的其他构造函数执行自己的初始化过程
```cpp
class Sales_data {
public:
   //非委托构造函数使用对应的实参初始化成员
   Sales_data(std::string s,unsigned cnt,double price):
      bookNo(s),units_sold(cnt),revenue(cnt*price){}
   //其余构造函数全都委托给另一个构造函数
   Sales_data():Sales_data("",0,0){}
   Sales_data(std::string s):Sales_data(s,0,0){}
   Sales_data(std::istream &is):Sales_data()
      {read(is,*this);} //委托默认构造函数
}
```
#### 7.5.3 默认构造函数的作用
- 默认初始化一下情况会发生：
   - 不适用任何初始值定义非静态变量或数组
   - 类本身含有类类型成员且使用合成默认构造函数
   - 类类型成员没有在构造函数初始化列表显示初始化时
#### 7.5.4 隐式类型转换
- 转换构造函数：构造函数只接受一个实参，则实际上定义了转换为此类型的隐式转换机制
- 只允许一步类类型转换
   ```cpp
   item.combine("9-999-999"); //错误
   item.combine(string("9-999-999"));  //正确
   ```
- 抑制构造函数定义的隐式转换
   ```cpp
   class Sales_data{
      explicit Sales_data(std::istream&);
   }
   ```
   - explicit 关键字只对一个实参的构造函数有效
   - 只能在类内部声明构造函数时使用，类外不能重复定义
   - explicit构造函数只能用于直接初始化
- 为转换显示使用构造函数
   ```cpp
   item.combine(Sales_data(nukk_book));//正确
   item.combine(static_cast<Sales_data>(cin));//正确
   ```
#### 7.5.5 聚合类
> 用户可以直接访问其成员，且具有特殊的初始化语法形式，类似C语言中的struct
- 所有成员是public
- 没有定义任何构造函数
- 没有类内初始值
- 没有基类、virtual函数
   ```cpp
   struct Data{
      int ival;
      string s;
   }
   Data val={0,"Anna"};
   ```
#### 7.5.6 字面值常量类
- 字面值类型的类可能含有constexpr函数成员，成员必须符合constexpr函数的所有要求，他们是隐式const的
- 数据成员都是字面值类型的聚合类是字面值常量类，不是聚合类但满足以下要求也是字面值常量类：
   - 成员都是字面值类型
   - 至少含有有一个constexpr构造函数
   - 若成员有类内初始值，则必须是常量表达式、或成员使用自己的constsxpr构造函数初始化
   - 类必须使用析构函数的默认定义
- constexpr构造函数：一般来说应该是空的
```cpp
class Debug{
public:
   constexpr Debug(bool b=true):hw(b),io(b),other(b){}   //必须初始化所有数据成员
   ...
}
```
## 7.6 类的静态成员
> 有时候类需要一些成员与类本身相关，而非与类各个对象保持联系
```cpp
class Account{
public:
   void calculate(){amount += amount*interestRate;}//成员函数可以直接访问
   static double rate(){return interestRate;}
   static void rate(double);
private:
   std::string owner;
   double amount;
   static double interestRate;
   static double initRate();
}
//在外部定义静态成员时不能重复使用static关键字
void Account::rate(double newRate){
   interestRate=newRate;
}
//定义并初始化静态成员
double Account::interestRate=initRate();
```
- 类的静态成员存在于任何对象之外，对象中不包含任何于静态成员有关的数据
- 静态成员函数也不与任何对象绑定一起，不包含this指针，不能声明为const
- 使用静态成员：
   ```cpp
   Account c1;
   double r=Account::rate();  //使用作用域运算符访问静态成员
   r=c1.rate();   //通过对象访问
   ```
- 静态函数的类内初始化
   ```cpp
   class Account{
   private:
      static constexpr int period=30;
      double daily_tbl[period];
   }
   //即使在类内初始化了，类外部也该定义一下
   constexpr int Account::period;
   ```
- 静态数据成员可以是不完全类型
- 静态数据成员可以作为默认实参















