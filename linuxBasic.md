#### 1.经常忘记的几个linux命令
- ```find . -name "*.c" -exec cat {} \; -o -name "*.h" -exec cat {} \;|sed '/^$/d'|wc -l ```：统计代码行数
- ```sort -t ‘:’ -k 3 -n /etc/passwd```	:对密码文件根据用户ID排序，-t指定区分键位置字符，-k排序其实位置
- ```tar -zxvf	file.tgz```	:解压用gzip压缩过的文件
- ```grep -r hzq /etc```:递归查找包含hzq的文件
- 查看文件的几个常用命令:
   - more、less、tail -n 2、head  -n 2
- ```ps -el --forest```:e,显示所有进程，l,显示进程详细信息，--forest,显示进程间的递归关系，-L,显示进程间的线程
- ```type hzq```
   - 输出：hzq is aliased to `clear;cal | boxes -d diamonds -p a1t2l3 | boxes -a c -d scroll|lolcat;'
- ```alias ll=‘ls -alF’	```:设置别名，不过仅在被定义的shell中才有效,不加参数查看可用别名
- ```printenv HOME```:查看单个环境变量,set可以查看局部变量，export可以将局部变量导入全局

---

---
### ```一、Unix的历史```

- 1965-```Multics```：Bell(贝尔实验室,后退出)、MIT(麻省理工学院)、GE(通用电器),要让大型主机可以达成提供 300 个以上的终端机连线使用
- 1969.8-```Unics```(UNIX原型)：作者Thompson用Assembler语言出了一组核心程序，同时包括一些核心工具程序， 以及一个小小的文件系统
- 1973-```UNIX```： Thompson、Ritchie等用C语言编写
- 1977-```BSD```( Berkeley Software Distribution)：柏克莱大学的 Bill Joy(SUN的创办者)，FreeBSD就是BSD的改版
- 1979-```System V```： AT&T 自家的 System V 、 IBM 的 AIX 以及 HP 与 DEC 等公司， 都有推出自家的主机搭配自己的 Unix 操作系统，并宣告版权

- 1984~1986-```Minix```： Andrew Tanenbaum(谭宁邦)从 1979 年的 Unix 第七版派生
- 1984~1990-```GNU与FSF基金```：史托曼(stallman)开始 GNU 计划， 这个计划的目的是：创建一个自由、开放的 Unix 操作系统（ Free Unix ），并编写了```GNU C Compiler(gcc)```(```Emacs```赚钱了之后)，成立FSF后，还撰写了更多可以被调用的C函数库(```GNU C library```)，以及可以用来操作操作系统的```BASH shell```，1985年他与律师草拟了有名的通用公共许可证（ General Public License, GPL ）
- 1988-```XFree86```： 1984 年由 MIT 与其他协力厂商首次发表了 ```X Window System``` ，并且更在 1988 年成立了非营利性质的 XFree86 这个组织
- 1991-```linux```：芬兰的赫尔辛基大学的 Linus Torvalds 在 BBS 上面贴了一则消息， 宣称他以 bash, gcc 等 GNU 的工具写了一个小小的核心程序，可在 Intel 的 386 机器上面运行

---
### ```二、主机规划与磁盘分区```
- 1.硬件设备

    |设备|设备在 Linux 内的文件名|
    |--|--|
    SCSI/SATA/USB 硬盘机| /dev/sd[a-p]
    USB 闪存盘| /dev/sd[a-p] （与 SATA 相同）
    VirtI/O 界面| /dev/vd[a-p] （用于虚拟机内）
    软盘机| /dev/fd[0-7]
    打印机|/dev/lp[0-2] （ 25 针打印机）<p>/dev/usb/lp[0-15] （ USB 接口）
    鼠标|/dev/input/mouse[0-15] （通用）<p>/dev/psaux （ PS/2 界面）<p>/dev/mouse （当前鼠标）
    CDROM/DVDROM|/dev/scd[0-1] （通用）<p>/dev/sr[0-1] （通用， CentOS 较常见）<p>/dev/cdrom （当前 CDROM ）
    IDE 硬盘机| /dev/hd[a-d] （旧式系统才有）
- 2.磁盘分区
   - 磁盘接口(personal PC)： ```SATA``` 、 ```SAS```
   - SATA/USB/SAS 等磁盘接口都是使用 SCSI 模块来驱动，因此设备名都是```/dev/sd[a-p]```，需要根据 Linux 核心侦测到磁盘的顺序来决定文件名
   - 磁盘：
      - 盘片：扇区(```Sector```,512/4K Bytes)，磁道(```Track```)
      - 机械手臂
      - 磁头
      - 主轴马达
      > 所有盘片的同一个磁道我们称为柱面(```Cylinder```)，常是文件系统的最小单位，也常是分区的最小单位<br>
      > 第一个扇区记录了整个磁盘的重要信息，有MBR(```Master Boot Record```)格式和GPT格式(```GUID partition table```)
   - MSDOS(```MBR```)分区表格式与限制
      - 早期linux为了兼容win使用 MBR 格式来处理开机管理程序与分区表
      > 第一个扇区512Bytes常会有这两个数据
      > - 1、主要开机记录区(MBR)：安装开机管理程序的地方，有 446 Bytes
      > - 2、分区表(partition table)：记录整颗硬盘分区的状态，64Bytes
      >    - 仅能有四组记录区，记录起始与结束的柱面号码
      >    - 假设硬盘的设备文件名是```/dev/sda```，那么四个分区文件名为```/dev/sda[1~4]```
      >    - 这四组分区信息我们称为主要（ Primary ）或延伸（ Extended ）分区
      >    - 系统的性能考虑：数据集中了，将有助于数据读取的速度与性能
      >    - 实际使用的是[延伸分区](http://en.wikipedia.org/wiki/Extended_boot_record)：每个分区的前面几个扇区用来记载分区信息
      >      - 由延伸分区继续切出来的分区，称为逻辑分区(```logical partition```)
      >      - 逻辑分区设备名从```/dev/sda5```开始
      >      - 逻辑分区有```2T的限制```，不知道为啥有这个限制
   - GUID partition table, ```GPT``` 磁盘分区表
      - GPT将磁盘所有的区块以LBA(512 Bytes)来规划，第一个称为LBA0
        > LBA：Logical Block Address，定义扇区(兼容512/4K Bytes)
      - GPT使用34个LBA区块来记录分区信息，最后33个LBA也拿来作为备份
      - LBA0
         > 与 MBR 模式相似的，这个相容区块也分为两个部份，一个就是跟之前 446 Bytes 相似的区块，储存了第一阶段的开机管理程序！而在原本的分区表的纪录区内，这个相容模式仅放入一个特殊标志的分区，用来表示此磁盘为 GPT 格式之意。而不懂 GPT 分区表的磁盘管理程序， 就不会认识这颗磁盘，除非用户有特别要求要处理这颗磁盘，否则该管理软件不能修改此分区信息，进一步保护了此磁盘.
      - LBA1
         > 记录了分区表本身的位置与大小，同时记录了备份GPT分区位置，分区表检验机制码```CRC32```，
      - LBA2-33
         > 实际记录分区信息处，512Bytes放4笔记录，所以一个的最大容量为2^64*512Bytes=8ZB<br>
         > ```fdisk```不认识GPT,得```gdisk/parted```指令才行
      ---
      ---
      > 开机检测程序：目前的主机系统在载入硬件驱动方面的程序，主要有早期的 BIOS 与新的 UEFI 两种机制
   - __```BIOS 搭配 MBR/GPT```__
      - ```CMOS是记录各项硬件参数且嵌入在主板上的存储器，BIOS则是写入到主板上的一个固件，即开机时系统主动执行的第一个程序```
      - BIOS 会分析计算机有哪些存储设备，并依据设置去取得能够开机的硬盘，且到该硬盘去读取第一个扇区的MBR位置(446 Bytes)的开机管理程序(```boot loader```)。
         - 开机管理程序(```boot loader```)目的是载入核心文件(因为是在安装操作系统时提供的，所以认识硬盘的文件系统格式，所以能读取核心文件)
            - 提供菜单：使用者可选择不同的开机项目
            - 载入核心文件：直接指向可开机的程序区段
            - 转交其他loader
      > 如果使用类似```grub```的```boot loader```，就得额外分出一个```BIOS boot```分区，此分区才能放置其他其他开机过程所需的源码<br>
      > ```boot loader```除了放在MBR，还可以放在每个分区的卡机扇区(```boot sector```)
   - __```UEFI BIOS 搭配 GPT```__
      - GPT可提供64bit的寻址，但BIOS仅为16位程序，还得通过GPT提供相容模式才能读写磁盘，所以需要UEFI(```Extensible Firmware Interface```)，也叫UEFI BIOS(使用C程序语言)，比组合语言的BIOS更易开发
      - UEFI可直接取得GPT的分区表，不过最好保留BIOS boot分区支持
      - 为了兼容windows，必须格式化一个vfat文件系统，提供512M~1G左右的容量，从而让其他UEFI执行方便
      - __因为UEFI BIOS、BIOS boot、UEFI支持的分区，所以```/boot```得是```/dev/sda3```以后的文件名了__
        ![](./picture/kaiji.png)
      - 并不是所有bootload都支持GPT分区方式
        ![](./picture/shaoxie.png)
      ---
   - 磁盘分区的选择
      - 目录树结构，```如何结合目录树的架构与磁盘内的数据呢?```
        > 挂载：利用目录为进入点，将磁盘分区的数据放置在该目录下，即进入目录就是读取该分区，进入的目录叫做```挂载点```，最重要的根目录一定要被挂载到某个分区
- 3.参考资料与延伸阅读
    - [GUID](https://zh.wikipedia.org/wiki/%E5%85%A8%E5%B1%80%E5%94%AF%E4%B8%80%E6%A0%87%E8%AF%86%E7%AC%A6)：
        - win操作系统用GUID标识COM对象中的类和界面，脚本可以不知道DLL的位置，直接通过GUID来激活类或对象
        - 英特尔的GPT使用GUID来标识硬盘和分区
        - 微软的ActiveX使用UUID来标识每一个不同的浏览器控件
        - 数据库表格主键

    - [Linux磁區配置從頭開始 搞定MBR、GPT與UEFI](http://www.netadmin.com.tw/netadmin/zh-tw/technology/23D09E63D4CD46349410CDA0E36FC465)


### ```三、安装CentOS 7.x```
#### 3.1 练习机的规划
- 目前的linux环境，磁盘不超过2T默认使用MBR来处理分区
    - 预计分区情况
        |所需目录/设备|磁盘容量|文件系统|分区格式|
        |--|--|--|--|
        |BIOS boot|2MB|系统自订|主分区|
        | /boot | 1GB | xfs | 主分区 |
        | / | 10GB | xfs | LVM 方式 |
        | /home | 5GB | xfs | LVM 方式 |
        | swap | 1GB | swap | LVM 方式 |
- 开机管理系统(boot loader)：CentOS7.x 默认的grub2
#### 3.2 开始安装CentOS 7
- 调整开机媒体(我的微星主板是开机时按DEL键进BIOS)
    - 可以用下面的命令来烧写镜像
        ```css
        # 假设你的 USB 设备为 /dev/sdc ，而 ISO 文件名为 centos7.iso 的话：
        pi@raspberrypi:/home/ $ dd if=centos7.iso of=/dev/sdc
        ```
- 选择安装模式与开机
    - TABLE键-> 输入 inst.gpt
- 语言
- 软件
- 磁盘：most important
- boot loader、网络、时区、密码
##### 3.2.22、单一分区分区完成详细项目示意图
- ext2/ext3/ext4：Linux早期的文件系统类型，34多了日志记录，对系统复原较快
- swap：磁盘仿真成为内存，不会用到目录树的挂载，以前建议所需内存的2倍，现在磁盘一般足够大，1~2G就好，不过一般用不到了，用到了表示内存不够用
- BIOS Boot：GPT分区表可能会用到，MBR不需要
- xfs：Centos默认文件系统，适合大容量磁盘管理
- vfat：同时被linux与windows所支持
- 安装时可指定[核心参数]()：```nofb apm=off acpi=off pci=noacpi```
    - apm（Advanced Power Management）是早期的电源管理模块
    - acpi（Advanced Configuration and Power Interface）则是近期的电源管理模块
    - nofb则是取消显卡上面的缓冲内存侦测



---
### ```七、linux磁盘与文件管理系统```
#### __```1.认识文件系统```__
- 磁盘组成：盘片、机械手臂、主轴马达、扇区、柱面、格式(MBR/GPT)、dev/vd[a-d]
- 特性：存放文件属性、实际内容
- 常称 一个可被挂载的数据为一个文件系统(而不是一个分区)
- 区块：
   - superblock：整体信息、inode/block总量、使用量、剩余量
   - inode：属性
   - block：文件内容、大时需要好几个

<br> __EXT2文件系统__
- 为了管理庞大的 inode与block,ext2在格式化时区分了多个区块群组
      ![ext2示意图](./picture/ext2.jpg)
- 区块群组：
   - data block：放文件内容
      
      |block大小|1kB|2KB|4KB|
      |--|--|--|--|
      最大文件|16G|256G|2T
      最大总容量|2T|8T16T
   - inode table：
   - inode
      - 存取模式(r、w、e)
      - 拥有者与群组
      - 容量
      - 创建或状态改变的时间
      - 最近一次读取的时间
      - 文件特性标志
      - 真正内容指向
      - 大小为128Bytes、ext4为256Bytes
      - 记录的总block的大小=直接+间接+双间接+三间接
      ![inode](./picture/inode.png)
   - superblock
      - block与inode总量
      - 未/已使用的inode/block数量
      - inode与block大小
      - 挂载时间、最近一次写入时间、最近一次检验磁盘时间等
      - vaild bit：是否已被挂载
      - 一般为1K
      - 每个block group可能含有superblock的备份
      - ```filesystem description```：每个block group的开始与结束block号码、每个区段的block号码(superblock、bitmap、inodemap、data block)
      - ```block bitmap```：区块对照表，区块是否已用
      - ```dumpe2fs```：查看superblock信息的指令
      - __```blkid```__：对系统块设备所使用的文件系统类型、LABEL、UUID等进行查询
      - __```dumpe2fs```__：显示ext2、ext3、ext4的超级块和块组信息

<br> __与目录树的关系__
- 目录：分配一个inode与至少一个block
   - inode：目录相关权限与属性
   - block：目录下的文件名与占用该inode号码数据
- 文件：一个inode与相对该文件大小的block

<br> __挂载点的意义__
- 每个文件系统有独立的inode/block/superblock等，链接到目录树才能被使用，结合的过程叫做挂载。point：```挂载点一定时目录，作为文件系统的入口。```
- linux支持的常见文件系统
   - 传统文件系统：ext2/minix/MS-DOS/FAT(vfat模块)/iso9660(光盘)
   - 日志式：ext3/ext4/ReiserFS/Windows'NTFS/IBM's JFS/SGI's XFS/ZFS
   - 网络文件系统：NFS/SMBFS
- 查看linux支持哪些文件系统：```ls -l /lib/modules/$(uname -r)/kernel/fs```
- 已载入到内存中支持的文件系统：```cat /proc/filesystems```
- linux通过 VFS来管理文件系统：
   ![](./picture/vfs.gif)

#### __```2.文件系统的简单操作```__
- 磁盘与目录容量
   - df：列出文件系统的整体磁盘使用量(常用：```df -ahT、df /home/ -hT```)
      - a  ：列出所有的文件系统，包括系统特有的 /proc 等文件系统；
      - k  ：以 KBytes 的容量显示各文件系统；
      - m  ：以 MBytes 的容量显示各文件系统；
      - h  ：以人们较易阅读的 GBytes, MBytes, KBytes 等格式自行显示；
      - H  ：以 M=1000K 取代 M=1024K 的进位方式；
      - T  ：连同该 partition 的 filesystem 名称 （例如 xfs） 也列出；
      - i  ：不用磁盘容量，而以 inode 的数量来显示
   - du：评估文件系统的磁盘使用量(常用：```sudo du -sh /*```)
      - -a  ：列出所有的文件与目录容量，因为默认仅统计目录下面的文件量而已。
      - -h  ：以人们较易读的容量格式 （G/M） 显示；
      - -s  ：列出总量而已，而不列出每个各别的目录占用容量；
      - -S  ：不包括子目录下的总计，与 -s 有点差别。
      - -k  ：以 KBytes 列出容量显示；
      - -m  ：以 MBytes 列出容量显示

#### __```3.磁盘的分区、格式化、检验与挂载```__
- ```lsblk -[dfimpt] [device]```：/sys/dev/block文件
<br>(常用：```lsblk -fp```)
   - b：仅列出磁盘本身
   - f：列出磁盘内文件系统名称
   - m：设备在/dev下的权限名称
   - p：设备的完整文件名
   - t：磁盘的详细数据：贮列机制、预读数据量等
- ```sudo parted /dev/vda print```：列出磁盘相关数据
- ```partprobe -s```：更新核心分区表
- 磁盘格式化(创建文件系统) mkfs
   - .
- ```blkid ```：显示目前系统有被格式化的设备
- ```dumpe2fs /dev/vda5```：如果/dev/vda5分区是ext2格式，查看信息

<br> __文件系统的挂载__
- 单一文件系统不应该被重复挂载在不同的挂载点
- 单一目录不应该重复挂载多个目录
- 要作为挂载点，理论上应该是空目录才是
- ```mount```
   - -a  ：依照配置文件 [/etc/fstab](../Text/index.html#fstab) 的数据将所有未挂载的磁盘都挂载上来
   - -l：增列 Label名称
   - -t：指定欲挂载的文件系统类型，不指定时将
      - /etc/filesystem指定测试挂载文件系统类型的顺序
      - /proc/filesystem已经载入的文件系统类型
      - /lib/modules/$(uname -r)/kernel/fs/
   - -n：将实际情况写入 /etc/mtab 中
   - -o：接额外参数，如账号密码
- ```我们也能利用mount将某个目录挂载到另外一个目录```
   - 这不是挂载文件系统，而是额外挂载某个目录的方法
   - 也可以用symbolic link来连接
      ```css
      pi@raspberrypi:/home/hzq $ sudo mount --bind /home/hzq/ /hzq/home/
      pi@raspberrypi:/home/hzq $ ls -lid /home/hzq/ /hzq/home/
      392482 drwxrwxrwx 6 hzq hzq 4096 8月  30 12:02 /home/hzq/
      392482 drwxrwxrwx 6 hzq hzq 4096 8月  30 12:02 /hzq/home/
      ```
- umount
   - -f：强制卸载
   - -l：立即卸载
   - -n：不更新 /etc/mtab
- 题外话：[如何在ubuntu上安装nfs服务](https://vitux.com/install-nfs-server-and-client-on-ubuntu/)
- tune2fs
   - -l：类似dumpe2fs -h，将superblock内的数据读出来
   - -L：修改LABEL name
   - -U：修改UUID

#### __```4.设置开机挂载```__
- 编辑文件 ```/etc/fstab```
   ```css
   [设备/UUID等]  [挂载点]  [文件系统]  [文件系统参数]  [dump]  [fsck]
   ```
- 测试语法 ```mount -a```
- 特殊设备loop挂载->大小固定
```css
ll -h /tmp/CentOS-7.0-1406-x86_64-DVD.iso
mkdir /data/centos_dvd
mount -o loop /tmp/CentOS-7.0-1406-x86_64-DVD.iso /data/centos_dvd
df /data/centos_dvd
```


