#include <iostream>
#include <memory>
#include <string>
class Test {
public:
    Test(){std::cout<<"structure"<<std::endl;}
    ~Test(){std::cout<<"destruct"<<std::endl;}
};
int main()
{
    std::unique_ptr<Test> p1(new Test());
    //std::unique_ptr<int> p2=p1;
    std::unique_ptr<Test> p2=nullptr;
    //p1=nullptr;
    p1.reset();
    std::cout<<"before return"<<std::endl;
    return 0;
}

/// 12.7
/**
 * a.错误，编译错误
 * b.对
 * c.对
 * d.错误，试图释放栈空间
 * e.对
 * f.错误，同一个指针会被释放两次
 */