#include <iostream>
#include <map>
#include <string>
#include <algorithm>
#include <cctype>
using namespace std;
void word_count_pro(std::map<std::string, int> &m)
{
    std::string word;
    while (std::cin >> word)
    {
        for (auto &ch : word)
            ch = tolower(ch);

        word.erase(remove_if(word.begin(), word.end(), [](char ch) { return ch == '.'; }),
                   word.end());
        ++m[word];
    }
    for (const auto &e : m)
        std::cout << e.first << " : " << e.second << "\n";
}
int main()
{

    // map<string, size_t> word_count;
    // string word;
    // while (cin >> word)
    // {
    //     ++word_count[word];
    // }
    // for (const auto &w : word_count)
    //     cout << w.first << " occurs " << w.second << ((w.second > 1) ? "times" : "time") << endl;
    std::map<std::string, int> m;
    word_count_pro(m);
    return 0;
}