#include <memory>
#include <list>
#include <vector>
#include <string>
#include <new>
#include <iostream>
using namespace std;

class strBlob
{
public:
    typedef std::vector<std::string>::size_type size_type;
    strBlob() : data(make_shared<vector<string>>()) {}
    strBlob(std::initializer_list<std::string> il) : data(make_shared<vector<string>>(il)){};
    size_type size() const { return data->size(); }
    bool empty() const { return data->empty(); }
    /// 添加元素
    void push_back(const std::string &t) { data->push_back(t); }
    void pop_back()
    {
        check(0, "pop on empty strBlob");
        data->pop_back();
    }
    /// 元素访问
    std::string &front()
    {
        /// 为空则抛异常
        check(0, "front on empty strBlob");
        return data->front();
    }
    /// 声明 const 属性, 表示不会改变内容
    std::string &front() const
    {
        /// 为空则抛异常
        check(0, "front on empty strBlob");
        return data->front();
    }
    std::string &back()
    {
        check(0, "back on empty strBlob");
        return data->back();
    }

private:
    std::shared_ptr<std::vector<std::string>> data;
    /// 如果 data[i] 不合法,抛出一个异常
    void check(size_type i, const std::string &msg) const
    {
        if (i >= data->size())
            throw std::out_of_range(msg);
    }
};

int main()
{
    vector<string> v1;
    {
        vector<string> v2 = {"a", "an", "the"};
        v1 = v2;
        v2.push_back("about");
    }
    cout << "v1.size() = " << v1.size() << endl;
    strBlob b1;
    {
        /// 新的作用域
        strBlob b2 = {"a", "an", "the"};
        b1 = b2;
        b2.push_back("about");
    }
    cout << "b1.size() = " << b1.size() << endl;
    return 0;
}