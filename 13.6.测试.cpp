#include <memory>
#include <string>
#include <iostream>
using namespace std;
int main()
{
    int i =42;
    const int &r = i;
    //int &&rr = i;
    //int &r2 = i*42;
    const int &r3 = i*42;
    cout<<i<<"->"<<r3<<endl;
    int &&rr2 = i*42;
    cout<<i<<"->"<<rr2<<endl;
    i++;
    rr2++;
    cout<<i<<"->"<<rr2<<endl;
    string s1="aaa",s2="bbb";
    s1+s2="wow";
    cout<<s1<<s2<<endl;
    return 0;
}