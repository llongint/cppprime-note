
# 14.重载运算与类型转换

## 14.1 基本概念
- 重载运算符是一种特殊名字的函数，由关键字 operator和要定义的运算符共同组成
    - 一元运算符一个参数
    - 二元两个：左侧传递给第一个，右侧第二个
    - 若是成员函数，则第一个(左侧)绑定到this指针上
        - 比运算符函数少一个参数
    - 或者是类成员，或者至少包含一个类型参数
    - 四个符号(+、-、*、&)既是一元运算符也是二元运算符
- 直接调用一个重载的运算符函数
    ```{.cpp}
    data1 + data2;
    operator+(data1 + data2);
    
    data1 += data2;
    data1.operator+=(data2);
    ```
- 某些运算符不应该被重载
    - 一些运算符无法保留求值顺序：
        - `逻辑与、逻辑或、逗号运算符、&&、||`
    - 一些有内置的特殊含义,可能导致类用户无法适应
        - `逗号、取地址运算符`
- 使用与内置类型一致的含义
    - 逻辑上与运算符有关的操作才重载，否则应定义成函数
        - IO 操作的移位运算符与内置类型的IO保持一致
        - 相等性逻辑应定义 `operator==`，通常应同时包含 `operator!=`
        - 单序比较操作应定义 `opeartor<`，同时应该包含其他关系操作
        - 返回类型应与内置版本的返回类型兼容：
            - 逻辑、关系运算返回 bool
            - 算术运算返回一个类类型的值
            - 赋值、复合赋值运算符(`+=`)应返回左侧运算对象的一个引用
- 选择作为成员或非成员
    - 赋值(`=`)、下标(`[]`)、调用(`()`)、成员访问(`->`)运算符必须是成员
    - 复合赋值一般应是成员，但非必须
    - 改变运算状态或与类型密切相关的运算符(递增、递减、解引用)通常应是成员
    - 对称性或可能转换任意一端的运算对象(算术、相等性、关系、位运算)通常应是普通的非成员函数
        - 想提供含有类对象的混合类型表达式、运算对象必须定义成非成员函数
        - 运算对象定义成成员函数时，左侧运算对象必须是所属类的一个对象
            ```{.cpp}
            string u = "hi" + s;    // 是成员函数则出错
            ```
## 14.2 输入和输出运算符
### 14.2.1 重载输出运算符<<
- 形式
    - 通常第一个形参是ostream对象的引用
    - 第二个形参一般来说是一个常量引用
    - 一般返回 ostream 形参
- sales_data 的输出运算符
```{.cpp}
ostream *operator<<(ostream &os, const sales_data &item) {
    os<<item.isbn()<<" "<<item.units_sold<<" "<<item.revenue<<" "<<item.avg_price();
    return os;
}
```
- 输出运算符尽量减少格式化操作
    - 不应打印换行符
- 输入输出运算符必须是非成员函数
    - 否则左侧运算对象是类的对象

### 14.2.2 重载输入运算符 >>
- 通常形式
    - 第一个形参是运算符将要读取的流的引用
    - 第二个形参是将要读入的对象的引用
    - 返回给定流的引用
- sales_data 的输入运算符
    ```{.cpp}
    istream *operator>>(ostream &is, const sales_data &item) {
        double price;
        is>>item.bookNo>>item.units_sold>>price;
        if(is)
            item.revenue = item.units_sold*price;
        else
            item = sales_data();
        return is;
    }
    ```
- 输入时错误
    - 可能存在下列错误情况：
        - 欲读取数字，但输入不是数字，则后续操作都将失败
        - 读取操作达到文件尾或者遇到输入流错误也会失败
- 标示错误
    - 通常只设置 failbit，设置 eofbit 标示文件耗尽，badbit 标示流被破坏

## 14.3 算术和关系运算符
### 14.3.1 相等运算符
- 设计准则：
    - 相等操作应定义成 `operator==` 而不是其他函数名
    - `operator==` 应该能判断对象是否有重复数据
    - 应具有传递性
    - 若定义了 `operator==`，也应定义 `operator!=`
    - 上述应该一个的工作托福给另一个
### 14.3.2 关系运算符
- 关系运算符通常应该
    - 定义顺序
    - 含有 ==运算符的，若不等，则其一应<另一个
## 14.4 赋值运算符
- 除了拷贝赋值与移动赋值，还有第三种赋值
    ```{.cpp}
    class StrtVec {
    public:
        StrtVec &operator=(std::initializer_list<std::string> li) {
            auto data = alloc_n_copy(li.begin(),li.end());
            free();
            elements = data.first;
            first_free = cap = data.second;
            return *this;
        }
    }
    ```
    - 不检查自赋值是因为确保了li与this所指的不是一个对象
- 复合赋值运算符
    - 不非得是类成员，但建议是
        ```{.cpp}
        sales_data &sales_data::operator+=(const sales_data &rhs) {
            units_sold += rhs.units_sold;
            revenue +=rhs.revenue;
            return *this;
        }
        ```
- 下标运算符
    - 通常同时定义常量和非常量版本
    ```{.cpp}
    class strVec {
    public:
        std::string &operator[](std::size_t n) 
            {return elements[n];}
        const std::string &operator[](std::size_t n) const
            {return elements[n];}
    }
    ```
## 14.6 递增、递减运算符
- 建议设定为成员函数

- 定义前置递增/递减运算符
    ```{.cpp}
    class strBlobPtr {
    public:
        strBlobPtr &operator++() {
            check(curr,"increment past end of strBlobPtr");
            ++curr;
            return *this;
        }
        strBlobPtr &operator--() {
            --curr;
            check(curr,"decrement past begin of strBlobPtr");
            return *this;
        }
    }
    ```
- 区分前置和后置运算符
    - 为了区分前置，使用一个int形参，通常不使用
        ```{.cpp}
        class strBlobPtr {
        public:
            strBlobPtr &operator++(int) {
                strBlobPtr ret = *this;
                ++*this;
                return ret;
            }
            strBlobPtr &operator--() {
                strBlobPtr ret = *this;
                --*this;
                return ret;
            }
        }
        ```
- 显示调用后置运算
    ```{.cpp}
    strBlobPtr p(a1);
    p.operator++(0);    // 后置版本
    p.operator++();     // 前置版本
    ```
## 14.7 成员访问运算符
```{.cpp}
class strBlobPtr {
public:
    std::string &operator*() const {
        auto p = check(curr, "dereference past end");
        return (*p)[curr];
    }
    std::string *operator->() const {
        return &this->operator*();
    }
};
strBlobPtr a1 = {"hi","bye","now"};
strBlobPtr p(a1);
*p = "okey";    // 首元素赋值
(*p).size() == p->size();   // 永远相等
```
- 注意这里加了 const 限定符
- 对箭头运算符返回值的限定
    - 箭头获取成员这一事实永远不变(可变从不同成员获取)
    - 下面三个方式等价
        ```{.cpp}
        point->mem
        (*point).mem
        point.operator()->mem
        ```
    - 必须返回类的指针或自定义了箭头运算符的某个类的对象
## 14.8 函数调用运算符
- 即重载函数调用运算符(函数对象) `operator()`
- 含有状态的函数对象类
    ```{.cpp}
    class printString {
    public:
        printString(ostream &o = cout, char c=' '):os(o),sep(c){}
        void operator()(const string &s)const{os<<s<<sep;}
    private:
        ostream &os;
        char sep;
    };
    printString printer,errors(cerr,'\n');
    printer(s);
    errors(s);
    ```
    - 常结合 for_each 一起使用
        ```{.cpp}
        for_each(vs.begin(),vs.end(),printString(cerr,'\n'));
        ```
### 14.8.1 lambda 是函数对象
- `[](const string &a, const string &b){return a.size() < b.size();}` 类似于：
    ```{.cpp}
    class shortString {
    public:
        bool operator()(const string &s,const string &s2) const
            {return s1.size() < s2.size();}
    };
    ```
- 表示 lambda 及相应捕获行为的类
    - 含捕获行为即含相应的数据成员
### 14.8.2 标准库定义的函数对象
- 包含在头文件 `<functional>`
    - `plus<T>、equal_to<T>、logical_and<T>、minus<T>、、、`
- 在算法中使用标准库函数对象
    - 如想要降序：
        ```{.cpp}
        sort(v.begin(),v.end(),greater<string>())
        ```
    - 如想要按照地址来排序
        ```{.cpp}
        vector<string *> nameType;
        sort(nameType.begin(),nameType.end(),less<string *>());
        ```
### 14.8.3 可调用对象与 function
- 两个不同的类型可能共享一种调用形式(一个函数类型)
- 不同类型可能具有相同的调用形式
    - 可用函数表来存储指向这些可调用对象的"指针"
        ```{.cpp}
        map<string,int(*)(int,int)> binops;
        binops.insert({"+",add});
        ```
- 标准库 function 类型
    ```{.cpp}
    map<string, function<int(int,int)>> binops = {
        {"+",add},//函数指针
        {"-",std::minus<int>()},//标准库函数对象
        {"/",divide()},//用户定义的函数对象
        {"*",[](int i,int j){return i*j;}},//lambda表达式
        {"%",mod}   //命了名的lambda
    };
    binops["+"](10,5);
    ```
- 重载的函数与 function
    - 可通过函数指针解决同名函数问题
    - 也可通过 lambda 来消除二义性

## 14.9 重载、类型转换与运算符
- 转换构造函数和类型转换运算符共同定义了 `类类型转换`
### 14.9.1 类型转换运算符
- 特殊成员函数：负责将类类型转换成其他类型
    ```{.cpp}
    operator type const;
    ```
    - type 要能作为函数的返回类型
- 定义含有类型转换运算符的类
    ```{.cpp}
    class samllInt {
    public:
        smallInt(int i=0):val(i) {
            if(i<0||i>255)throw std::out_of_range("bad smallInt value");
        }
        operator int() const {return val;}
    private:
        std::size_t val;
    };
    smallInt si = 4;//将4隐式转换成smallInt
    si+=3;// 将si隐式转换成int，再执行整数的加法
    ```
    - 注意必须是成员函数、不能有返回类型、参数列表为空
- 类型转换运算符可能产生意外结果
    - 书上给的例子不妥，总之不要使用让人意外的类型转换
- 显示的类型转换运算符
    ```{.cpp}
    class samllInt {
    public:
        explicit operator int() const {return val;}
    };
    samllInt si=4;
    si+=3;//错误
    static_cast<int>(si) + 3;//正确
    ```
    - 有一个例外是作为条件时，编译器会将类型转换自动应用于它
- 转换为 bool
    - 无论我们在什么时候在条件中使用流对象，都会使用为IO类型定义的 operator bool:
        ```{.cpp}
        while(std::cin>>value)
        ```
### 14.9.1 避免有二意性的类型转换
- 有两种情况可能产生二义性
    - A类有接受B类的转换构造，B也有A的
    - A定义了多个转换规则，涉及的转换类型本身可通过其他类型装换联系在一起
- 实参匹配和相同类型转换
    ```{.cpp}
    struct B;
    struct A {
        A()=default;
        A(const B&);
    };
    struct B {
        operator A() const;
    };
    A f(const A&);
    B b;
    A a = f(b);//二义性错误
    ```
    - 可改成显示调用：
        ```{.cpp}
        A a1=f(b.operator A());//使用显示类型转换
        A a2=f(A(b));//使用构造
        ```
- 二义性与转换目标为内置类型的多重类型转换
    ```{.cpp}
    struct A {
        A(int=0);
        A(double);//最好不要创建两个转换源都是算术类型的类型转换
        operator int()const;
        operator double()const;
    };
    void f2(long double);
    A a;
    f2(a);//二义性错误
    long lg;
    A a2(lg);//二义性错误
    ```
    - `提示：`除了显示向bool类型的转换外，我们应该尽量避免定义类型转换函数并尽可能限制那些 "显然正确" 的非显示构造函数
- 重载函数与转换构造函数
    ```{.cpp}
    struct C {
        C(int);
    };
    struct D {
        D(int);
    };
    void mainip(const C&);
    void mainip(const &D);
    mainip(10);//二义性错误
    ```
    - 可显示调用构造函数解决
        ```{.cpp}
        mainip(C(10));
        ```
- 重载函数与用户定义的类型转换
    ```{.cpp}
    struct C {
        C(int);
    };
    struct E {
        E(double);
    };
    void mainip(const C&);
    void mainip(const E&);
    mainip(10);//二义性错误,可以是mainip(C(10))或mainip(E(double(19)))
    ```
### 14.9.3 函数匹配与重载运算符
```{.cpp}
class smallInt {
    std::size_t val;
    friend smallInt opeartor+(const smallInt&,const smallInt&);
public:
    smallInt(int=0);
    operator int()const{return val;}
};
smallInt s1,s2;
smallInt s3=s1+s2;
int i=s3+0; // 二义性错误
```












