#include <cstdio>
#include <cstring>
#include <vector>
#include <list>
#include <iostream>
#include <array>

using namespace std;

// 类型参数前必须用关键字 class 或 typename,含义相同
template <class T>
// 类型参数可指定返回类型和参数类型
T compare(const T &v1, const T &v2)
{
    // 类型参数可声明变量或类型转换
    if (v1 < v2)
        return (T)(-1);
    if (v2 < v1)
        return (T)(1);
    return (T)(0);
}

// 编译器会使用字面常量来代替N和M,从而实例化模板
template <unsigned N, unsigned M>
int compa(const char (&p1)[N], const char (&p2)[M])
{
    cout << "N==" << N << ",M==" << M << endl;
    return strcmp(p1, p2);
}
template <typename T>
T min(const T &, const T &);
/// 练习16.4
template <class Iter, typename value>
Iter myfind(Iter first, Iter last, const value &v)
{
    for (; first != last; first++)
    {
        if (*first == v)
            return first;
    }
    return first;
}
/// 练习16.5
template <typename Arr>
void print(const Arr &arr)
{
    for (const auto &e : arr)
        cout << e << endl;
}
/// 练习16.6
template <typename T, unsigned int Len>
T *mybegin(T (&arr)[Len])
{
    return arr;
}
template <typename T, unsigned int Len>
T *myend(T (&arr)[Len])
{
    return arr + Len;
}

template <typename T, unsigned int Len>
constexpr int size(const T (&arr)[Len])
{
    return Len;
}

int main(int argc, char **argv)
{
    char p1[40] = "hi", p2[50] = "mon";
    cout << compa("hi", "mmon") << endl;
    cout << compa(p1, p2) << endl;

    /// test myfind()
    vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    cout << "find3:" << *myfind(v.begin(), v.end(), 3) << endl;

    list<int> l;
    l.push_back(1);
    l.push_back(2);
    l.push_back(3);
    l.push_back(4);
    cout << "find2:" << *myfind(l.begin(), l.end(), 2) << endl;

    /// test print()
    array<int, 8> a{1, 2, 3, 4, 5};
    print(a);

    int aa[8] = {1, 2, 3, 4, 5};
    print(aa);

    /// test mybegin()、myend()
    cout << "test mybegin()、myend():" << endl;
    for (auto p = mybegin(aa); p != myend(aa); p++)
    {
        cout << *p << "\t";
    }

    /// test size()
    cout << endl
         << "size of 'aa' is:" << size(aa) << endl;

    return 0;
}
