
# 13.拷贝控制
- 一个类通过五种特殊的成员函数来控制对象的拷贝、移动、赋值和销毁
    - 拷贝构造函数
    - 拷贝赋值运算符
    - 移动构造函数
    - 移动赋值运算符
    - 析构函数

## 13.1 拷贝、赋值与销毁
### 13.1.1 拷贝构造函数
- 第一个参数是自身的引用，且任何额外参数都有默认值
    ```{.cpp}
    class Foo {
        Foo(const Foo&);    // 拷贝构造函数
    };
    ```
    - 在几种情况下会被隐式使用，所以通常不应该是 explicit
- 合成拷贝构造函数
    - 即使定义了其他构造函数，编译器也会为我们合成一个拷贝构造函数
    - 编译器从给定的对象中依次将每个非static成员拷贝到正在创建的对象中
        - 类使用拷贝构造
        - 内置的直接拷贝
        ```{.cpp}
        sales_data::sales_data(const sales_data &orig):
            bookNo(orig.bookNo),
            units_sold(orig.units_sold),
            revenue(orig.revenue){}
        ```
- 拷贝初始化
    ```{.cpp}
    string dots(10,'.');    //直接初始化，普通函数匹配
    string dots = string(10,'.');   //拷贝初始化，将右侧的运算对象拷贝到正在创建的对象中
    ```
    - 使用等号
    - 对象做实参，传递给非引用类型的形参
    - 非引用类型函数返回一个对象
    - 花括号初始化数组元素或聚合类中的成员
- 参数和返回值
    - 函数具有非引用类型的返回类型时，返回值用来初始化调用方的结果
        - `自己的参数必须时引用类型`
- 拷贝初始化的限制
    - 初始值要求通过一个 explicit 的构造函数进行类型转换时：
    ```{.cpp}
    class CxString {  
    public:
        int _size;  char *_pstr; 
        explicit CxString(int size) {   // 使用关键字explicit的类声明, 显示转换 
            _size = size; 
        }
        CxString(const char *p) {  
            // 省略...  
        }
    }
    CxString string2 = 10;  // 这样是不行的, 因为explicit关键字取消了隐式转换
    CxString string5 = "bbb"; // 这样是OK的, 调用的是CxString(const char *p)
    string3 = string1;        // 这样是不行的, 因为取消了隐式转换, 除非类实现操作符"="的重载
    ```
- 编译器可以绕过拷贝构造函数
    - 编译器可以(但不是必须)绕过
        ```{.cpp}
        string book("11.11");   // 虽然绕过，但拷贝构造函数必须在且能访问
        ```
### 13.1.2 拷贝赋值运算符
- 未定义编译器会合成
- 重载赋值运算符
    - 本质上时一个函数
        - 名字是 `operator=`
        - 有返回类型和参数列表
    - 若运算符是成员函数
        - 左侧运算对象就绑定到隐式的 this 指针
        - 二元运算符的右侧作为显示参数传递
    - 编译器通常要求保存在容器中的类型具有赋值运算符，且返回值是左侧运算对象的引用
- 合成拷贝赋值运算符
    - 常用来禁止对象的赋值
    - 或将右侧每个非static对象成员赋予左侧，返回引用
### 13.1.3 析构函数
    - 成员函数，名字是波浪号接函数名，无返回值与参数
        - 不能被重载，唯一
- 析构函数的工作
    - 按初始化顺序逆序销毁成员
    - 销毁发生什么依赖于成员类型
- 什么时候调用析构函数(对象被销毁)
    - 离开作用域
    - 对象被销毁时，成员被销毁
    - 容器被销毁、成员被销毁
    - delete 时
    - 临时对象：创建的完整表达式结束时
- 合成析构函数
    - 未定义时编译器会自动定义
    - 本身不直接销毁成员：成员销毁步骤之外的另一部分进行的

### 13.1.4 三/五法则
- `需要析构函数的类也需要拷贝和赋值操作`
- `需要拷贝操作的类也需要赋值操作,反之亦然`

### 13.1.5 使用 `=default`
- 显示要求编译器生成合成版本
    ```{.cpp}
    sales_data {
        sales_data() = default;
        sales_data(const sales_data &) = default;
        sales_data & operator=(const sales_data &);
        ~sales_data() = default;
    };
    sales_data &sales_data：：operator=(const sales_data &) = default;
    ```
### 13.1.5 阻止拷贝
- 比如 iostream 类阻止了拷贝
- 定义删除的函数
    - 虽然声明了，但不能以任何方式使用它
    - `=delete` 必须出现在函数第一次声明的时候，可对任何函数使用
    ```{cpp}
    struct NoCopy {
        NoCopy() = default;
        NoCopy(const NoCopy&) = delete; // 阻止拷贝
        NoCopy &operator(const NoCopy&) = delete;   // 阻止赋值
        ~NoCopy() = default;  // 阻止使用合成析构
    };
    ```
- 析构函数不能是删除的成员
    - 成员不能被销毁
- 合成的拷贝构造函数可能是删除的
    - 有成员的`析构/拷贝构造、拷贝赋值运算符`是删除或不可访问的，或`引用成员或const成员没有类内初始化器`，则合成`析构/拷贝构造`时删除的
- private 拷贝控制
    - 将 拷贝构造函数或拷贝赋值运算符声明为 private 可阻止拷贝
        - 友元函数仍能拷贝对象，所以还是最好用 `=delete`

## 13.2 拷贝控制和资源管理
- 需先确定拷贝的语义：
    - 行为像值：有自己的状态，与副本完全独立
    - 行为像指针：共享状态，使用相同底层数据

### 13.2.1 行为像值的类
- 类值拷贝赋值运算符
    - 通常组合了析构、构造函数的操作
        - 似析构：会销毁左侧运算对象的资源
        - 似拷贝构造：会从右侧运算对象拷贝数据
            - 确保将对象赋予自身也正确
            - 异常安全
```{.cpp}
HasPtr &HasPtr::operator=(const HasPtr &rhs) {
    auto newp = new string(*rhs.ps);
    delete ps;      // 释放旧内存
    ps = newp;      // 从右侧运算对象拷贝数据到本对象
    i = rhs.i;
    return *this;   // 返回本对象
}
```
### 13.2.2 定义行为像指针的类
- 最好的办法是使用 `shared_ptr` 管理类中的资源
    - 直接管理的话可用 `引用计数`
- 引用计数
    - 工作方式
        - 记录共享状态的对象个数
        - 拷贝构造函数不分配新的计数器
        - 析构递减计数器，变0则释放
        - 拷贝赋值运算符递增右侧计数器，递减左侧计数器
    - 不能直接作为数据成员
- 定义一个使用引用计数器的类
    ```{.cpp}
    class HasPtr {
    public:
        HasPtr(const string &s = string()):
            ps(new string(s)),i(0),use(new size_t(1)){}
        HasPtr &operator=(const HasPtr&);
        ~HasPtr();
    private:
        string *ps;
        int i;
        size_t *use;
    }
    ```
- 类指针的拷贝成员"篡改"引用计数
    ```{.cpp}
    HasPtr::~HasPtr(){
        if(--*use == 0) {
            delete ps;
            delete use;
        }
    }
    ```
    ```{.cpp}
    HasPtr &HasPtr::operator=(const HasPtr &rhs) {
        ++*rhs.use;
        if(--*use == 0){
            delete ps;
            delete use;
        }
        ps=rhs.ps;
        i=rhs.i;
        use = rhs.use;
        return *this;
    }
    ```
## 13.3 交换操作
- 交换元素时常用 swap
    - 一次拷贝两次赋值(需避免不必要的内存分配)
        - 希望交换指针而非分配新副本
- 编写自己的 `swap`
    - 典型实现
        ```{.cpp}
        calss HasPtr {
            friend void swap(HasPtr&, HasPtr&);
        }
        inline swap(HasPtr &lhs,HasPtr &rhs) {
            using std::swap;
            swap(lhs.ps, rhs.ps);
            swap(lhs.i,rhs.i);
        }
        ```
- swap() 应调用 swap(),而不是 std::swap()
    - 如果内置成员有自己的swap
        ```{.cpp}
        void swap(Foo &lhs, Foo &rhs) {
            using std::swap;
            swap(lhs.h,rhs.h);  // 如果 HasPtr定义了swap，会优先用 HasPtr的(匹配优先级高)
        }
        ```
- 在赋值运算符中使用 swap
    - 拷贝并交换技术(自动异常安全、且正确处理自然赋值)
        ```{.cpp}
        // 注意 rhs 按值传递, 即 HasPtr的拷贝构造函数将右侧运算对象的string 拷贝到 rhs
        HasPtr &HasPtr::operator=(HasPtr rhs) {
            // 交换左侧和局部变量
            swap(*this,rhs);    // rhs 现在指向本对象曾经使用过的内存
            return *this;       // rhs 被销毁
        }
        ```
## 13.4 拷贝控制示例
- 待回头细看

## 13.5 动态内存管理类
- 待回头细看

## 13.6 对象移动
- 某些情况下，移动而非拷贝会大幅提升性能
### 13.6.1 右值引用
- 必须绑定到右值的引用
    - 通过 `&&` 来获得右值引用
    - 只能绑定到一个要销毁的对象
    > 一般而言，左值表达式表示对象身份，右值表达式表示对象的值
    - 可以绑定到一个表达式，但不能绑定到一个左值上(与左值引用相反)
        ```{.cpp}
        int i=42;
        const int &r3 = i*42;
        int &&rr2 = i*42;
        ```
        > 左值表达式：返回 `左值引用` 的函数，赋值、下标、解引用和前置递增/递减运算符，可绑定到 `左值引用`
        ><br> 右值：返回非引用类型的函数，算数、关系、位运算符及后置递增/递减运算符，都生成右值，可绑定到 `const的左值引用`
- 左值持久，右值短暂
    - 右值要么是字面值常量，要么是表达式求值过程中创建的临时对象
        - 所引用的对象将要被销毁
        - 该对象没有其他用户
- 变量是左值
    - 变量：只有运算对象，没有运算符的表达式，都是左值
    - 所以不能将一个右值引用直接绑定到一个变量上
- 标准库 move 函数
    - 定义在 `<utility>`
    - 获得绑定到左值上的右值引用
        ```{.cpp}
        int &rr3=std::move(rr1);
        ```
        - 调用 move 即承诺不再使用 rr1
            - 可销毁 `移后源`，也可赋新值，但不能使用对象的值
        - move 不提供 using 声明
### 13.6.2 移动构造函数和移动赋值运算符
- 移动构造函数的第一个参数是该类的一个 `右值引用`
- 需确保移后源销毁后是无害的
    ```{.cpp}
    strVec::strVec(strVec &&s) noexcept
        :elements(s.elements),first_free(s.first_free),cap(s.cap) {
            // 使s:运行析构是安全的
            s.element = s.first_free = s.cap = nullptr;
        }
    ```
- 移动操作、标准库容器异常
    - 移动操作'窃取'资源，如果不抛异常，必须标记为'noexcept', 应通知标准库避免额外的操作
    - `noexcept` 需同时加在头文件和源文件
    - 注意：为了满足发生异常时，旧容器保持不变的要求，我们必须在明确知道元素类型不会抛异常的情况下才能用移动构造函数，否则应使用拷贝构造函数，
- 移动赋值运算符
    - 同样，不抛异常就应该标记为 `noexcept`, 同时必须正确处理自赋值
        ```{.cpp}
        strVec &strVec:operator=(const strVec &&rhs) noexcept {
            if(this != &rhs) {
                free();
                elements= rhs.elements;
                first_free = rhs.first_free;
                cap = rhs.cap;
                rhs.elements = rhs.first_free = rhs.cap = nullptr;
            }
            return *this;
        }
        ```
- 移后源对象必须可析构
    - 移动操作对移后源留下的值没有要求，所以应该不依赖移后源对象的值
- 合成的移动操作
    - 没错，编译器会自动为某些类合成
        - 当类没有定义任何自己版本的拷贝控制成员，且类的非static数据成员读能移动
            - 如果数据成员是内置类型或类成员没有移动操作
            ```{.cpp}
            struct X {
                int i;
                std::string s; // string 有自己的移动操作
            }
            struct hasX {
                X men;  // X 有合成的移动操作
            };
            X x,x2 = std::move(x);  // 使用合成移动构造
            hasX hx, hx2 = std::move(hx);   // 使用合成移动构造
            ```
        - 移动操作永远不会定义为删除函数，除非使用`=defaule`且有的成员不能移动
            - 有类成员定义了拷贝构造但未定义移动构造，或编译器不能为其合成移动构造函数
            - 有类成员的移动构造函数或移动赋值运算符定义为删除或不可访问的
            - 类的析构函数被定义为删除的
            - 有类成员是 const 或引用
    - 注意：若定义了自己的移动构造/赋值运算符，则合成拷贝构造和拷贝赋值运算符会被定义为删除的
- 移动右值，拷贝左值
    - 如果类既有移动构造，也有拷贝构造，那使用普通的函数匹配规则，移动构造只适用于非static右值的情形
        ```{.cpp}
        strVec v1,v2;
        v1=v2;
        strVec getVec(istream &);//getVec返回一个右值
        v2 = getVec(cin);   // getVec(cin)是一个右值，使用移动赋值
        ```
- 如果没有移动构造函数，右值也会被拷贝
    - 因为可将 `Foo&&` 转换为 `const Foo&` 
        - 几乎肯定是安全的
- 拷贝并交换赋值运算符和移动操作
    ```{.cpp}
    class HasPtr {
    public:
        HasPtr(HasPtr &&p) noexcept:ps(p.ps),i(p.i){p.ps=0;}
        HasPtr operator=(HasPtr rhs)
            {swap(*this,rhs);return *this;}
    }
    ```
    - 注意赋值运算赋是一个非引用参数，依赖于移动拷贝构造或拷贝构造
- 三五法则：
    - 五个拷贝控制成员应该看成一个整体，定义了任何一个就应该定义全部五个
- Message 类的移动操作
    ```{.cpp}
    void Message::move_Folders(Message *m) {
        folders = std::move(m_>folders);    // 使用set的移动赋值运算符
        for(auto f:folders) {
            f->remMsg(m);   // 删除旧Message
            f->addMsg(this);// 将本Message添加到Folder中
        }
        m->folders.clear(); // 确保销毁m是无害的
    }
    ```
    - 添加元素可能会抛 `bad_alloc()` 异常，所以函数未标记为 `noexcept`
    ```{.cpp}
    Message::Message(Message &&m):contents(std::move(m.contents)) {
        move_Folders(&m);
    }
    ```
    ```{.cpp}
    Message &Message::operator=(Message &&rhs) {
        if(this != &rhs) {
            remove_from_Folders();
            contents = std::move(rhs.contents);
            move_Floders(&rhs);
        }
        return *this;
    }
    ```
- 移动迭代器
    - 通过改变给定迭代器的解引用运算符来适配迭代器。
    - 移动迭代器的解引用运算符生成一个右值引用
    - 标准库提供了 `make_move_iterator()` 返回移动迭代器
    ```{.cpp}
    void strVec::reallocate() {
        // 分配两倍规模的空间
        auto newcapacity = size() ? 2*size():1;
        auto first = alloc.allocate(newcapacity);
        // 移动元素
        auto last = uninitialized_copy(make_move_iterator(begin()),
            make_move_iterator(end()), first);
        free();
        elements = first;
        first_free = last;
        cap = elements + newcapacity;
    }
    ```
    - `再次强调`：只有在确信算法在为一个元素赋值或将其传递给一个用户定义的函数后不再访问它时，才能将移动迭代器传递给算法。
- `头发有限，暂时先跳过 13.6.2 节的练习，头发多了记得补上啊!!!`

### 13.6.3 右值引用和成员函数
- 通常定义一个接受 `const X&`  和 `X&&` 参数的版本
    ```{.cpp}
    class strVec {
    public:
        void push_back(const std::string&);
        void push_back(std::string&&);
    }
    void strVec::push_back(const string &s) {
        chk_n_alloc();
        alloc.consturct(first_free++,s);
    }
    void strVec::push_back(string &&s) {
        chk_n_alloc();
        alloc.construct(first_free++,std::move(s));
    }
    vec.push_back("done");
    ```
- 右值和左值引用成员函数
    - 允许向右值赋值
        ```{.cpp}
        string s1="aaa",s2="bbb";
        s1+s2="wow";
        ```
        - 阻止这种用法的方法是使用`引用限定符`（&或&&）
            ```{.cpp}
            Foo &operator=(const Foo &) &;
            Foo anotherMen() const &;   // 同时拥有两个限定符
            ```
- 重载和引用函数
    ```{.cpp}
    class Foo {
    public:
        Foo sorted() && {
            sort(data.begin(),data.end());
            return *this;
        }
        Foo sorted() const & {
            Foo ret(*this); // 拷贝一个副本
            sort(ret.data.begin(),ret.data.end());
            return ret;
        }
    private:
        vector<int> data;
    }
    ```
    - 相同函数相同变量名的函数要么都加引用限定符，要么都不加



