#include <iostream>
#include <iterator>
#include <vector>
using namespace std;
int main()
{
    vector<int> vec = {1,2,3,4,5,6};
    ostream_iterator<int> out_iter(cout," ");
    for(auto &e:vec){
        out_iter  = e;
    }
    cout<<endl;
    copy(vec.begin(),vec.end(),out_iter);
    cout<<endl;
    return 0;
}