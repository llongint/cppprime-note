#include <memory>
#include <list>
#include <iostream>
using namespace std;
int main()
{
    shared_ptr<string > p1;
    /// 判断 p1 是否指向一个对象
    cout<<(p1?"NOT NULL":"NULL")<<endl;
    /// 获取指向的对象
    p1 = make_shared<string>("12345");
    if(p1 && !p1->empty())
        cout<<*p1<<endl;
    shared_ptr<list<int> > p2;
    return 0;
}
