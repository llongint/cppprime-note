#include <string>
class Sales_data {
//为类的非成员函数做友元声明
// Sales_data的非成员接口函数
friend Sales_data add(const Sales_data&,const Sales_data&);
friend std::ostream &print(std::ostream&,const Sales_data&);
friend std::istream &read(std::istream,Sales_data&);

public:  //添加访问说明符
   //构造函数
   Sales_data() = default;
   Sales_data(const std::string &s):bookNo(s){}
   Sales_data(const std::string &s,unsigned n,double p):
      bookNo(s),units_sold(n),revenue(p*n){}
   Sales_data(std::istream &);

   //新成员：关于Sales_data对象的操作
   std::string isbn() const {return bookNo;}
   Sales_data &combine(const Sales_data&);
private:
   double avg_price() const
      {return units_sold?revenue/units_sold:0;}
   //数据成员
   std::string bookNo;
   unsigned units_sold = 0;
   double revenue = 0.0;
};

// Sales_data的非成员接口函数
Sales_data add(const Sales_data&,const Sales_data&);
std::ostream &print(std::ostream&,const Sales_data&);
std::istream &read(std::istream,Sales_data&);



