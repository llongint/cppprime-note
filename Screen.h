#ifndef _SCREEN_H
#define _SCREEN_H

#include <string>

class Screen{
public:
   // 必须先定义后使用
   typedef std::string::size_type pos;
   // 类型别名，与上等价
   // using pos = std::string size_type;
   Screen() = default;//已有构造函数，不加不会自动生成默认构造函数
   Screen(pos ht,pos wd,char c):height(ht),width(wd),
      contents(ht *wd,c){}
   char get() const
      {return contents[cursor];} //去读光标处字符，隐式内联
   inline char get(pos ht,pos wd) const;  //显示内联
   Screen &move(pos r,pos c);    // 能在之后被设置为内联
   void some_member() const;
   Screen &set(char);
   Screen &set(pos, pos, char);
   //根据对象是否时const重载函数
   Screen &display(std::ostream &os)
      {do_display(os);return *this;}
   const Screen &display(std::ostream &os) const
      {do_display(os);return *this;}
private:
   pos cursor = 0;
   pos height = 0, width = 0;
   std::string contents;
   mutable size_t access_ctr;
   void do_display(std::ostream &os) const{os<<contents;}//隐式内联函数
};
inline Screen &Screen::move(pos r,pos c){
   pos row = r*width;
   cursor = row + c;
   return *this;
}
char Screen::get(pos r,pos c)const  //在类内声明成inline
{
   pos row = r*width;      //计算行的位置
   return contents[row + c];  //返回给定列字符
}
void Screen::some_member()const{
   ++access_ctr;
}
inline Screen &Screen::set(char c){
   contents[cursor] = c;
   return *this;
}
inline Screen &Screen::set(pos r,pos col,char ch){
   contents[r*width + col] = ch;
   return *this;  //将this对象作为左值返回
}








#endif