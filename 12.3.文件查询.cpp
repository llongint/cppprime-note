#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <memory>
#include <map>
#include <set>
#include <sstream>
#include <fstream>
using namespace std;

/**
 * @brief 保存返回的结果：单词、出现次数、行号、及文本
 */
class queryResult
{
public:
    using lineNo = vector<string>::size_type;
    using iter = set<lineNo>::iterator;
    iter begin() const {return lines->begin();}
    iter end() const {return lines->end();}
    shared_ptr<vector<string>> get_file() const
    {
        return file;
    }
    friend ostream &print(ostream &os, const queryResult &qr);
    queryResult(string s,
                shared_ptr<set<lineNo>> p,
                shared_ptr<vector<string>> f) : sought(s), lines(p), file(f) {}

private:
    string sought;
    shared_ptr<set<lineNo>> lines;
    shared_ptr<vector<string>> file;
};
/**
 * @brief  保存输入文件的类
 */
class textQuery
{
public:
    using lineNo = queryResult::lineNo;
    textQuery(ifstream &infile) : pTexts(new vector<string>)
    {
        string text;
        while (getline(infile, text))
        {
            pTexts->push_back(text);
            int n = pTexts->size() - 1;
            istringstream line(text);
            string word;
            while (line >> word)
            {
                auto &lines = pResult[word]; // 下标运算会将word添加到map中
                if (!lines)
                    lines.reset(new set<lineNo>);

                lines->insert(n);
            }
        }
    }
    queryResult query(const string &s) const
    {
        auto loc = pResult.find(s);
        if (loc == pResult.end())
            return queryResult(s, shared_ptr<set<lineNo>>(new set<lineNo>), pTexts);
        return queryResult(s, loc->second, pTexts);
    }

private:
    shared_ptr<vector<string>> pTexts; // 保存文件内容
    map<string, shared_ptr<set<lineNo>>> pResult;
};

ostream &print(ostream &os, const queryResult &qr)
{
    os << qr.sought << " occurs " << qr.lines->size() << " "
       << "time" << (qr.lines->size() > 1 ? "s" : "") << endl;
    for (auto num : *qr.lines)
        os << "\t(line " << num + 1 << ") " << *(qr.file->begin() + num) << endl;
    return os;
}
/**
 * @brief  
 * @note   
 * @param  &infile: 指向要处理的文件
 * @retval None
 */
void runQueryies(ifstream &infile)
{
    textQuery tq(infile);
    while (true)
    {
        cout << "enter word to look for, or q to quit: ";
        string s;
        if (!(cin >> s) || s == "q")
            break;
        print(cout, tq.query(s)) << endl;
    }
}
int main()
{
    ifstream ifs("main.cxx");
    runQueryies(ifs);
    return 0;
}