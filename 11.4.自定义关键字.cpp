#include <iostream>
#include <map>
#include <unordered_map>
#include <fstream>
#include <sstream>
#include <functional>

using namespace std;
//bool eqOp(const Test &lhs, const Test &rhs);
struct Test
{
public:
    friend bool eqOp(const Test &lhs, const Test &rhs);
    Test(int aa, float bb, double cc) : a(aa), b(bb), c(cc) {}
    int a;
    float b;
    double c;
};
size_t hasher(const Test &sd)
{
    return hash<string>()(to_string(sd.a) + to_string(sd.b)) ^ hash<int>()(sd.c);
}
bool eqOp(const Test &lhs, const Test &rhs)
{
    return lhs.a == rhs.a && lhs.b == rhs.b && lhs.c == rhs.c;
}
void test1()
{
    using SD_map = unordered_map<Test, int, decltype(hasher) *, decltype(eqOp) *>;
    SD_map tt;
    Test ts(1, 2, 3);
    tt[ts] = 1;
}
void test2();
void test3();
int main()
{
    test1();
    test2();
    test3();
    return 0;
}
class Person
{
public:
    string name;
    int age;

    Person(string n, int a)
    {
        name = n;
        age = a;
    }

    bool operator==(const Person &p) const
    {
        return name == p.name && age == p.age;
    }
};

struct hash_name
{
    size_t operator()(const Person &p) const
    {
        return hash<string>()(p.name) ^ hash<int>()(p.age);
    }
};
void test2()
{
    unordered_map<Person, int, hash_name> ids; //不需要把哈希函数传入构造器
    ids[Person("Mark", 17)] = 40561;
    ids[Person("Andrew", 16)] = 40562;
    for (auto ii = ids.begin(); ii != ids.end(); ii++)
        cout << ii->first.name
             << " " << ii->first.age
             << " : " << ii->second
             << endl;
}
//----------------------------------- test3
typedef pair<string, string> Name;

namespace std
{
    template <> //function-template-specialization
    class hash<Name>
    {
    public:
        size_t operator()(const Name &name) const
        {
            return hash<string>()(name.first) ^ hash<string>()(name.second);
        }
    };
};
void test3()
{
    unordered_map<Name, int> ids;
    ids[Name("Mark", "Nelson")] = 40561;
    ids[Name("Andrew", "Binstock")] = 40562;
    for (auto ii = ids.begin(); ii != ids.end(); ii++)
        cout << ii->first.first
             << " " << ii->first.second
             << " : " << ii->second
             << endl;
}
