#include <iostream>
#include <string>
#include <vector>
using namespace std;

class base
{
public:
    base(string name) : basename(name) {}
    string name() { return basename; }
    virtual void print(ostream &os) { os << basename; }

private:
    string basename;
};
class derived : public base
{
public:
    friend void protTest() {
        //cout<<"baseName:"<<basename<<endl;
    }
    derived(string name, int ii = 0) : base(name), i(ii) {}
    void print(ostream &os)
    {
        base::print(os);
        os << " " << i;
    }

private:
    int i;
};

int main()
{
    derived D("test");
    D.print(cout);
    return 0;
}