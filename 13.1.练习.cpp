#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct X
{
	X() { std::cout << "X()" << std::endl; }
	X(const X&) { std::cout << "X(const X&)" << std::endl; }
	X& operator=(const X&) { std::cout << "X& operator=(const X&)" << std::endl; return *this; }
	~X() { std::cout << "~X()" << std::endl; }
};
void f(const X &rx, X x)
{
	std::vector<X> vec;
	vec.reserve(2);
	vec.push_back(rx);
	vec.push_back(x);
}
class HasPtr
{
public:
    HasPtr(const std::string &s = std::string()) : ps(new std::string(s)), i(0) {}
    HasPtr(const HasPtr &h):i(h.i),ps(new string(*h.ps)){}
    HasPtr& operator=(const HasPtr &h){
        i=h.i;
        std::string *new_ps = new std::string(*h.ps);
		delete ps;
		ps = new_ps;
        return *this;
    }
    ~HasPtr(){
        delete ps;
    }
private:
    std::string *ps;
    int i;
};
int main()
{
    X *px = new X;
	f(*px, *px);
	delete px;
    return 0;
}
