#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static int cmpstringp(const void *p1, const void *p2)
{
    return *(int *)p1 < *(int *)p2;
}
int test(const void *p1, const void *p2)
{
    return *(int *)p1 < *(int *)p2;
}
int main(int argc, char *argv[])
{
    int j = 0;
    int a[5] = {2, 4, 3};
    qsort(a, 5, sizeof(int), cmpstringp);

    for (j = 0; j < 5; j++)
        printf("%d ", a[j]);
    printf("\n");
    exit(EXIT_SUCCESS);
}
//output:
//5 4 3 2 1